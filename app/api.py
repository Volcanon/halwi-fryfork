import requests
import dateutil.parser
import datetime


headers = {'content-type': 'text/plain', 'Accept': 'application/json'}

# LucidBot
lucid = 'http://127.0.0.1:49998/api/'

#
def check_auth(username, password):
    try:
        return requests.get(lucid, auth=(username, password)).status_code == 200
    except:
        return False
#

#
def Kingdom(kid, username, password):
    # fetch
    fetch = lambda x: requests.get(lucid+x, auth=(username, password), headers=headers).json()
    
    main = {}
    
    main['type'] = 'Kingdom'
    
    # fetch kingdom
    kd = fetch('kingdoms/'+str(kid))
    
    #Dragon
    try:
        main['Dragon'] = kd['dragon']['name']
    except:
        pass
    
    
    # Kingdom Info
    main['name'] = kd['name']
    main['loc'] = kd['location']
    
    # fetch provinces
    p = fetch('provinces?kingdomId='+str(kid))
    provinces = []
    for i in p:
        province = i
        
        #adding 
        try:
            province['NWPA'] = int(province['networth']) / int(province['land'])
        except:
            province['NWPA'] = ''
        
        # -> Time
        t = str(datetime.datetime.now() - dateutil.parser.parse(province['lastUpdated'][:-6]))
        t = t[:t.find('.')]
        if 'day' not in t:
            t = '0d ' + t
        province['last'] = t[:t.find(',')]
        
        provinces.append(province)
        
    
    main['provinces'] = provinces
    main['kid'] = kid
    main['head'] = ['Province', 'Race', 'Personality', 'Acres', 'Networth', 'NWPA', 'Updated', 'ECO', 'ECD']
    
    
    # Title
    main['title'] = kd['name']+' '+kd['location']
    
    main['kd'] = kd
    
    return main

#

#
def Index(username, password):
    # fetch
    fetch = lambda x: requests.get(lucid+x, auth=(username, password), headers=headers).json()
    
    main = {}
    
    #links
    main['links'] = fetch('weblinks')
    
    main['title'] = 'HAL'
    return main

#

#
def Province(pid, username, password):
    # fetch
    fetch = lambda x: requests.get(lucid+x, auth=(username, password), headers=headers).json()
    
    main = {}
    
    p = fetch('provinces/'+str(pid))
    
    main['p'] = p
    
    try:
        main['t'] = fetch('intel/sots/'+str(p['sot']['id']))
    except:
        pass 
    try:
        main['m'] = fetch('intel/soms/'+str(p['som']['id']))
    except:
        pass
    try:
        main['s'] = fetch('intel/soss/'+str(p['sos']['id']))
    except:
        pass
    try:
        main['su'] = fetch('intel/surveys/'+str(p['survey']['id']))
    except:
        pass
    
    main['type'] = 'province'
    main['pid'] = pid
    main['title'] = p['name']
    return main
#


#
def AddIntel(intel, username, password):
    if 'Current kingdom is ' in intel:
        p = requests.post(lucid+'kingdoms/', data=intel, auth=(username, password), headers=headers).text
    else:
        p = requests.post(lucid+'intel/', data=intel, auth=(username, password), headers=headers).status_code
    if p == 200:
        return 'Posted Intel'
    elif p == 400:
        return 'Bad Intel'
    elif p == 500:
        return 'Something went wrong'
    else:
        return p
#

#
def Search(inp, username, password):
    # fetch
    fetch = lambda x: requests.get(lucid+x, auth=(username, password), headers=headers).json()

    kds = {i['location']: i['id'] for i in fetch('kingdoms')}
    provs = {i['name']: i['id'] for i in fetch('provinces')}

    if inp in kds:
        return 'kingdom', kds[inp]
    elif inp in provs:
        return 'province', provs[inp]
    




#