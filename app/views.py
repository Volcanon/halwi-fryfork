from app import app
from flask import render_template, request, Response, session, redirect
from api import check_auth, Kingdom, Index, AddIntel, Province, Search
from functools import wraps

#Secret key
app.secret_key = "SECRET CODE ? YOU FIX"


# Auth
def authenticate():
    return Response(
        'Could not verify your access level for that URL.\n'
        'You have to login with proper credentials', 401,
        {'WWW-Authenticate': 'Basic realm="Login Required"'})
    
def requires_auth(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        auth = request.authorization
        if not auth or not check_auth(auth.username, auth.password):
            return authenticate()
        return f(*args, **kwargs)
    return decorated
#


# Index
@app.route('/')
@requires_auth
def index():
    #session['user'] = request.authorization.username
    #session['pass'] = request.authorization.password
    return render_template('index.html', data=Index(request.authorization.username, request.authorization.password), s = session)
#

# Kingdom
@app.route('/kd/<int:kid>')
@requires_auth
def kingdom(kid):
    return render_template('kingdom.html',data=Kingdom(kid, request.authorization.username, request.authorization.password))
    
#

# Province
@app.route('/intel/<int:pid>')
@requires_auth
def province(pid):
    return render_template('province.html',data=Province(pid, request.authorization.username, request.authorization.password))
    
#


#
@app.route('/addintel', methods=['POST'])
@requires_auth
def addintel():
     s = request.form['intel']
     session['intel'] = str(AddIntel(s, request.authorization.username, request.authorization.password))
     return redirect('/')
#

#
@app.route('/search', methods=['POST'])
@requires_auth
def search():
     try:
        s = Search(request.form['search'], request.authorization.username, request.authorization.password)
        if 'province' == s[0]:
            return redirect('/intel/'+str(s[1]))
        elif 'kingdom' == s[0]:
            return redirect('/kd/'+str(s[1]))
        else:
            return redirect('/')
     except:
        return redirect('/')

     #return redirect('/')
#