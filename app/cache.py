import redis
import json
from api import Kingdom

# Connecting to Redis on localhost on default port.
r = redis.Redis()


#
def KingdomCache(kid, username, password):
    if 'kd:'+str(kid) in r.keys():
        return json.loads(r.get('kd:'+str(kid)))
    else:
        cache = Kingdom(kid, username, password)
        r.setex('kd:'+str(kid), json.dumps(cache), 300)
        return cache
#